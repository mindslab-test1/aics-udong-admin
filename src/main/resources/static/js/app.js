window.onload = function () {
    $(document).on("click", ".check_no_show > button", function () {

        let rowValue = $(this).val();
        let parentRow = $(this).closest("tr");
        let brother_btn = $(this).siblings();
        if (brother_btn.hasClass("active")) {
            brother_btn.removeClass("active");
        }

        parentRow.data("value", rowValue);
        $(this).addClass("active");
    })

    $(document).on("click", ".btn_complete", function () {
        let thisButton = $(this);
        let parentRow = $(this).closest("tr");
        let noshowId = parentRow.data("noshow_id");
        let noshowValue = parentRow.data("value");

        if (!noshowValue) {
            alert("노쇼 여부가 선택되지 않았습니다. 확인 해 주세요.");
            return;
        }

        console.log(noshowId, noshowValue);

        const data = {"noShowId": noshowId, "noShowValue": noshowValue};

        $.ajax({
            url: "/confirmNoShow",
            method: "POST",
            async: false,
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "json"
        }).done(function (data) {
            if (data.code === 200) {
                alert("정상적으로 처리 되었습니다.")
                parentRow.find("button").attr("disabled", "disabled");
                thisButton.html("처리완료");
            } else {
                parentRow.find("button").attr("disabled", "disabled");
                thisButton.html("에러발생");
            }
        })
    })

    $.ajax({
        url: "/getNoShowAllInfo",
        method: "POST",
        async: false,
        contentType: "application/json"
    }).done(function (data) {
        appendRow(data);
    });

    $(".btn_search").click(function () {
        const searchDate = parseSearchDate($("#search_date").val());
        console.log(searchDate);

        if (isNaN(parseInt(searchDate)) || searchDate.length != 8) {
            alert("올바른 날짜 형식으로 입력 해 주세요. YYYY.MM.DD");
            return;
        }
        let data = {"date": searchDate};

        $.ajax({
            url: "/getNoShowInfo",
            method: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function (data) {
            appendRow(data);
        })
    })
}

function parseSearchDate(dateValue) {

    return dateValue.replaceAll(".", "").replaceAll("-", "").replaceAll(" ", "");
}

function appendRow(data) {
    clearRow();
    let no_show_list = data;

    for (let index in no_show_list) {
        console.dir(no_show_list[index]);
        let new_row = "";

        try {
            new_row = parse_noshow(no_show_list[index]);
            console.log(new_row);
        } catch (error) {
            console.error(error);
        } finally {
            $('#noshow_row').append(new_row);
        }
    }
}

function clearRow() {
    $("#noshow_row").empty();
}

function parse_noshow(no_show_ele) {
    const schedule = no_show_ele.schedule;
    const service = schedule.service;
    const child = service.child;
    const address = service.addr;
    const teacher = service.teacher;

    return get_new_row(
        no_show_ele.noshowId,
        child.realName,
        child.birthDay,
        schedule.scheduleDate,
        schedule.startBlock.startTime,
        schedule.endBlock.endTime,
        (schedule.endBlock.timeBlockId - schedule.startBlock.timeBlockId + 1) * 0.5,
        address.addrAddress,
        service.phoneNumber,
        teacher.teacherName,
        teacher.teacherTel,
        no_show_ele.noshowFromUserYn,
        no_show_ele.noshowResultYn,
    );
}

function telStringFormatting(telString) {
    let midNumLength = 4;
    let endNumStart = 7;

    if (telString.length === 10) {
        midNumLength = 3;
        endNumStart = 6;
    }

    return [telString.substr(0, 3), telString.substr(3, midNumLength), telString.substr(endNumStart, 4)].join('-');
}

function dateStringFormatting(dateString) {
    const date = new Date(dateString.substr(0, 4), dateString.substring(4, 6), dateString.substring(6, 8));
    let month = '' + date.getMonth();
    let day = '' + date.getDate();
    let year = date.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('.') + getDay(date);
}

function getDay(date) {
    let dayString;
    console.log(date.getDay());

    switch (date.getDay()) {
        case 0 :
            dayString = "일";
            break;
        case 1 :
            dayString = "월";
            break;
        case 2 :
            dayString = "화";
            break;
        case 3 :
            dayString = "수";
            break;
        case 4 :
            dayString = "목";
            break;
        case 5 :
            dayString = "금";
            break;
        default :
            dayString = "토";
    }

    return "(" + dayString + ")";
}

function parseWho(whoKeyword) {
    if (whoKeyword === "Y") {
        return "P";
    }
    return "T";
}

function get_new_row(noshow_id, name, birthyear, classDay, startTime, endTime, classTime, address, p_phone, teacher, t_phone, noshow_req, noshow_result) {

    let btn_html;
    let result;
    let btn_result;
    console.group();
    console.log(classDay.substring(0, 4));
    console.log(classDay.substring(4, 6));
    console.log(classDay.substring(6, 8));
    console.groupEnd();

    let classDate = dateStringFormatting(classDay);
    let parentPhone = telStringFormatting(p_phone);
    let teacherPhtoe = telStringFormatting(t_phone);
    let whoNoShow = parseWho(noshow_req);

    if (noshow_result) {

        if (noshow_result === "Y" || noshow_result === "N") {
            btn_result = '<button type="button" class="btn_complete" disabled>처리완료</button></td>';
            if (noshow_result === "Y") {
                btn_html = '<button type="button" class="active" disabled>Y</button>\n<button type="button" disabled>N</button>'
            } else {
                btn_html = '<button type="button" disabled>Y</button>\n<button type="button" class="active" disabled>N</button>'
            }
        }
    } else {
        btn_result = '<button type="button" class="btn_complete">확인</button></td>';
        btn_html = '<button type="button" value="Y">Y</button>\n<button type="button" value="N">N</button>'
    }

    result = '<tr data-noshow_id="' + noshow_id + '">\
    <td><span class="name">' + name + '</span> (' + birthyear.substring(2, 4) + '년생)</td>\
    <td>' + classDate + '</td>\
    <td>' + startTime + '~' + endTime + '(' + classTime + '시간)</td>\
    <td class="elipsis" title="' + address + '">' + address + '</td>\
    <td>' + parentPhone + '</td>\
    <td>' + teacher + '</td>\
    <td>' + teacherPhtoe + '</td>\
    <td>' + whoNoShow + '</td>\
    <td class="check_no_show">' +
        btn_html
        + '</td>\
    <td>' + btn_result + '\n~\
    </tr>';

    return result;
}
