package ai.mindslab.udongAdmin.controller;

import ai.mindslab.udongAdmin.service.UdongAdminService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UdongAdminController {

    final
    UdongAdminService service;

    public UdongAdminController(UdongAdminService service) {
        this.service = service;
    }

    @GetMapping("/")
    public String index() {
        return "admin_no_show";
    }

}
