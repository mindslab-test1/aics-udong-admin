package ai.mindslab.udongAdmin.controller;

import ai.mindslab.udongAdmin.repositroy.dto.UdongConfirmNoShowParam;
import ai.mindslab.udongAdmin.service.UdongRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UdongAdminRestController {
    final UdongRestService service;
    final static Logger logger = LoggerFactory.getLogger(UdongAdminRestController.class);

    public UdongAdminRestController(UdongRestService service) {
        this.service = service;
    }

    @PostMapping(value = "/getNoShowAllInfo", consumes = "application/json", produces = "application/json")
    public String getNoShowAllInfo() {
        return service.getNoShowInfo("");
    }


    @PostMapping(value = "/getNoShowInfo", consumes = "application/json", produces = "application/json")
    public String getNoShowInfo(@RequestBody String date) {
        logger.info(date);

        return service.getNoShowInfo(date);
    }

    @PostMapping(value = "/confirmNoShow", consumes = "application/json", produces = "application/json")
    public String confirmNoShow(@RequestBody UdongConfirmNoShowParam param) {
        logger.info(param.toString());

        return service.confirmNoShow(param);
    }
}
