package ai.mindslab.udongAdmin.response;

import lombok.Data;

@Data
public class UdongResponse {
    private int code;
    private String message;

    public UdongResponse() {
        this.code = 200;
        this.message = "OK";
    }

    public UdongResponse(String message) {
        this.code = 500;
        this.message = message;
    }

    public UdongResponse(Exception e) {
        this(e.getMessage());
    }
}
