package ai.mindslab.udongAdmin.repositroy.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UdongNoshowDto {
    private int noshowId;
    private int scheduleId;
    private String noshowFromUserYn;
    private String noshowResultYn;
    private Date crtDate;
    private Date updDate;
    private UdongScheduleDto schedule;
}
