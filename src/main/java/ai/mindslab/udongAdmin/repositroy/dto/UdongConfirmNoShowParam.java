package ai.mindslab.udongAdmin.repositroy.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UdongConfirmNoShowParam {
    private int noShowId;
    private String noShowValue;
}
