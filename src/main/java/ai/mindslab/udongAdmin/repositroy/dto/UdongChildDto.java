package ai.mindslab.udongAdmin.repositroy.dto;

import lombok.Data;

@Data
public class UdongChildDto {
    private int childNo;
    private String realName;
    private String birthDay;
}
