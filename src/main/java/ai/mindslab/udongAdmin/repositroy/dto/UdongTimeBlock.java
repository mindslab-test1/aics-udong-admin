package ai.mindslab.udongAdmin.repositroy.dto;

import lombok.Data;

@Data
public class UdongTimeBlock {
    private int timeBlockId;
    private String startTime;
    private String endTime;
}
