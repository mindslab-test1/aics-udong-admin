package ai.mindslab.udongAdmin.repositroy.dto;

import lombok.Data;

@Data
public class UdongTeacherDto {
    private int teacherId;
    private int teacherCompanyNo;
    private String teacherName;
    private String teacherTel;
}
