package ai.mindslab.udongAdmin.repositroy.dto;

import lombok.Data;

@Data
public class UdongUserAddr {
    private int userAddrId;
    private int userId;
    private String addrAddress;
}
