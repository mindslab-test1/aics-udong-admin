package ai.mindslab.udongAdmin.repositroy.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UdongServiceDto {
    private int serviceId;
    private int userId;
    private int childId;
    private int userAddrId;
    private int teacherId;
    private int serviceState;
    private String phoneNumber;
    private int inicisId;
    private Date crtDate;
    private String serviceStartDate;
    private String serviceEndDate;
    private int serviceClassTime;
    private int timeBlockIdFrom;
    private int timeBlockIdTo;
    private UdongChildDto child;
    private UdongTeacherDto teacher;
    private UdongUserAddr addr;
}
