package ai.mindslab.udongAdmin.repositroy.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UdongScheduleDto {
    private int scheduleId;
    private int serviceId;
    private int scheduleClassTime;
    private Date crtDate;
    private Date updDate;
    private String scheduleCancelYn;
    private String scheduleChangeYn;
    private int scheduleState;
    private int timeBlockIdFrom;
    private int timeBlockIdTo;
    private String scheduleDate;
    private UdongServiceDto service;
    private UdongTimeBlock startBlock;
    private UdongTimeBlock endBlock;
}
