package ai.mindslab.udongAdmin.repositroy.mapper;

import ai.mindslab.udongAdmin.repositroy.dto.UdongConfirmNoShowParam;
import ai.mindslab.udongAdmin.repositroy.dto.UdongNoshowDto;

import java.util.List;

public interface UdongAdminMapper {
    List<UdongNoshowDto> selectUdongNoshow(String date);
    int updateNoShowState(UdongConfirmNoShowParam param);
}
