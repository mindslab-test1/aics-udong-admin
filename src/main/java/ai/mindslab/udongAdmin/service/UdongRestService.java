package ai.mindslab.udongAdmin.service;

import ai.mindslab.udongAdmin.repositroy.dto.UdongConfirmNoShowParam;
import ai.mindslab.udongAdmin.repositroy.dto.UdongNoshowDto;
import ai.mindslab.udongAdmin.repositroy.mapper.UdongAdminMapper;
import ai.mindslab.udongAdmin.response.UdongResponse;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UdongRestService {
    final UdongAdminMapper mapper;
    final Gson gson = new Gson();

    public UdongRestService(UdongAdminMapper mapper) {
        this.mapper = mapper;
    }

    public String getNoShowInfo(String data) {
        Map<String, String> dataMap = gson.fromJson(data, HashMap.class);
        String date;
        List<UdongNoshowDto> noShowList;
        try {
            date = dataMap.get("date");
            noShowList = mapper.selectUdongNoshow(date);
        } catch (NullPointerException exception) {
            noShowList = mapper.selectUdongNoshow(null);
        }
        return gson.toJson(noShowList);
    }

    public String confirmNoShow(UdongConfirmNoShowParam param) {
        int result;
        UdongResponse response = new UdongResponse();
        try {
            result = mapper.updateNoShowState(param);
        } catch (Exception exception) {
            result = 0;
            response = new UdongResponse(exception);
        }

        if (result != 1) {
            if (result == 0) response = new UdongResponse("Not Updated Data");
            else response = new UdongResponse("Multi Data Updated. Please Call Admin");
        }

        return gson.toJson(response);
    }
}
