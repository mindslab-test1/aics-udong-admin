package ai.mindslab.udongAdmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UdongAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(UdongAdminApplication.class, args);
    }

}
